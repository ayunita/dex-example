import React, { useEffect, useState } from "react";

import getWeb3 from "./getWeb3";
import ERC20 from "./abis/ERC20.json";
import Dex from "./abis/Dex.json";
import "./App.css";

const contractAddress = "0x014446F9e9e33d1Af3Ebe43FCE0dA14f6A1A22C4";
// const uniTokenAddress = "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984";
const oceanTokenAddress = "0x8967BCF84170c91B0d24D4302C2376283b0B3a07";
const linkTokenAddress = "0x01BE23585060835E02B77ef475b0Cc51aA1e0709";
function App() {
  const [web3, setWeb3] = useState(null);
  const [dexInstance, setDexInstance] = useState(null);
  const [depositAmount, setDepositAmount] = useState(0);
  const [swapAmount, setSwapAmount] = useState(0);
  const [depositMessage, setDepositMessage] = useState("");
  const [swapMessage, setSwapMessage] = useState("");
  const [account, setAccount] = useState(null);

  useEffect(() => {
    const createInstance = async () => {
      try {
        const _web3 = await getWeb3();
        if (_web3) setWeb3(_web3);

        // Use web3 to get the user's accounts.
        const accounts = await window.ethereum.request({
          method: "eth_accounts",
        });
        if (accounts && accounts.length > 0) {
          setAccount(accounts[0]);
        }

        const _dexInstance = new _web3.eth.Contract(
          Dex.abi, // Smart contract abi
          contractAddress // Smart Contract address
        );

        if (_dexInstance) {
          setDexInstance(_dexInstance);
        }
      } catch (e) {
        alert("Failed to load web3, accounts, or contract.");
        console.log(e);
      }
    };
    createInstance();
  }, []);

  const getAmount = (amount) => {
    // To handle big number issue, convert number to string
    // Ref: https://stackoverflow.com/questions/57380815/how-to-include-large-numbers-in-transactions-1e21
    return (amount * 10 ** 18).toString();
  };

  const approve = async (tokenAddress, amount) => {
    const erc20Instance = new web3.eth.Contract(
      ERC20.abi, // Smart contract abi
      tokenAddress // Smart Contract address
    );

    try {
      let result = await erc20Instance.methods
        .approve(contractAddress, getAmount(amount))
        .send({ from: account });
      if (result) {
        if (result.status) {
          return true;
        }
      }
    } catch (e) {
      return false;
    }
  };

  const deposit = async () => {
    try {
      setDepositMessage(`Loading ...`);

      let approval = await approve(oceanTokenAddress, depositAmount);

      if (approval) {
        let result = await dexInstance.methods
          .deposit(oceanTokenAddress, getAmount(depositAmount))
          .send({ from: account });
        if (result) {
          if (result.status) {
            setDepositMessage(
              `https://rinkeby.etherscan.io/tx/${result.transactionHash}`
            );
          }
        }
      }
    } catch (e) {
      console.log("Deposit: ", e);
      setDepositMessage(`Check error on dev console`);
    }
  };

  const swap = async () => {
    try {
      setSwapMessage(`Loading ...`);

      let approval = await approve(linkTokenAddress, swapAmount);

      if (approval) {
        let result = await dexInstance.methods
          .swap(
            linkTokenAddress,
            oceanTokenAddress,
            getAmount(swapAmount),
            getAmount(swapAmount)
          )
          .send({ from: account });
        if (result) {
          if (result.status) {
            setSwapMessage(
              `https://rinkeby.etherscan.io/tx/${result.transactionHash}`
            );
          }
        }
      }
    } catch (e) {
      console.log("Swap: ", e);
      setSwapMessage(`Check error on dev console`);
    }
  };

  return (
    <div className="App">
      <h2>Example of deposit/swap LINK</h2>
      <div style={{ background: "yellow", padding: 8 }}>
        <h3 style={{ color: "red" }}>This is using Rinkeby testnet!</h3>
        <a
          href={`https://rinkeby.etherscan.io/address/${contractAddress}`}
          target="_blank"
          rel="noreferrer"
        >
          Click here to see Dex contract on explorer
        </a>
        <p>LINK Contract Address: 0x01BE23585060835E02B77ef475b0Cc51aA1e0709</p>
        <p>
          OCEAN Contract Address: 0x8967BCF84170c91B0d24D4302C2376283b0B3a07
        </p>
      </div>
      <h3>Deposit LINK to Dex</h3>
      <p>
        (1) Tell ERC20 Token contract to: Give approval to my contract to
        transfer X amount of this ERC20 Token
      </p>
      <p>
        (2) Tell Dex contract to: Transfer my token from my account to Dex
        contract
      </p>
      <p>
        <a href={depositMessage} target="_blank" rel="noreferrer">
          {depositMessage}
        </a>
      </p>
      <p>
        {" "}
        <input
          type="number"
          value={depositAmount}
          onChange={(e) => setDepositAmount(e.target.value)}
        />{" "}
        LINK
      </p>
      <button className="pretty-button" onClick={() => deposit()}>
        Deposit
      </button>
      <h3>Swap LINK/OCEAN</h3>
      <p>
        (1) Tell ERC20 Token contract to: Give approval to my contract to
        transfer X amount of this ERC20 Token
      </p>
      <p>
        (2) Tell Dex contract to: Deposit source token and transfer target token
        to my account
      </p>
      <p>
        <a href={swapMessage} target="_blank" rel="noreferrer">
          {swapMessage}
        </a>
      </p>
      <p>
        Send:{" "}
        <input
          type="number"
          value={swapAmount}
          onChange={(e) => setSwapAmount(e.target.value)}
        />{" "}
        LINK, Receive: {swapAmount} OCEAN
      </p>
      <button className="pretty-button" onClick={() => swap()}>
        Swap to OCEAN (Price 1 LINK = 1 OCEAN)
      </button>
    </div>
  );
}

export default App;
