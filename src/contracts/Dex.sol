// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";

// LINK Rinkeby: 0x01BE23585060835E02B77ef475b0Cc51aA1e0709
// UNI Rinkeby: 0x1f9840a85d5af5bf1d1762f925bdaddc4201f984
contract Dex {
    
    struct Pool {
        address owner;
        address source;
        address target;
        uint256 sourceAmount;
        uint256 targetAmount;
    }
    
    mapping(address => Pool) public pools;
    
    mapping(address => mapping(address => uint256)) private pairsBalance;
    
    function deposit(address tokenAddress, uint amount) public {
        require(amount > 0, "Dex: deposit amount needed");
        require(ERC20(tokenAddress).transferFrom(msg.sender, address(this), amount), "Dex: transferFrom failed");
    }
    
    function swap(address source, address target, uint256 sourceAmount, uint256 targetAmount) public {
        require(sourceAmount > 0, "Dex: swap source amount needed");
        require(targetAmount > 0, "Dex: swap target amount needed");
        require(ERC20(source).transferFrom(msg.sender, address(this), sourceAmount), "Dex: swap transferFrom failed");
        require(ERC20(target).transfer(msg.sender, targetAmount), "Dex: swap transfer failed");
    }

}