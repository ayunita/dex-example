# Example for Swap/Deposit ERC20 Token

LIVE DEMO (Rinkeby testnet): [https://dex-dapp-sample.netlify.app/](https://dex-dapp-sample.netlify.app/)

## Getting Started

### Prerequisites
* node/npm
* ETH and LINK on Rinkeby
	* LINK: [https://rinkeby.chain.link/](https://rinkeby.chain.link/)
	* ETH: [https://faucet.rinkeby.io/](https://faucet.rinkeby.io/)

### Installation
Install all dependencies
```
npm install
```

Run client
```
npm run start
```